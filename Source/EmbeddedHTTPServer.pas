Unit EmbeddedHTTPServer;

{ Threadded HTTP Server Unit (c) 2014 Paul F. Michell, Michell Computing. }

{$Mode OBJFPC}
{$LongStrings ON}

Interface

Uses
  Classes, SysUtils, fpHTTPServer;

Type THTTPServerThread = Class(TThread)
  Private
    FServer: TFPHTTPServer;
  Public
    Constructor Create(APort: Word; Const OnRequest: THTTPServerRequestHandler);
    Procedure Execute; Override;
    Procedure DoTerminate; Override;
    Property Server: TFPHTTPServer Read FServer;
  End;

Implementation

Constructor THTTPServerThread.Create(APort: Word; Const OnRequest: THTTPServerRequestHandler);
Begin
  FServer := TFPHTTPServer.Create(Nil);
  FServer.Port := APort;
  FServer.OnRequest := OnRequest;
  Inherited Create(False);
End;

Procedure THTTPServerThread.Execute;
Begin
  Try
    FServer.Active := True;
  Finally
    FreeAndNil(FServer);
  End;
End;

Procedure THTTPServerThread.DoTerminate;
Begin
  Inherited DoTerminate;
  FServer.Active := False;
End;

End.

