Unit Main;

{ Allez Fencing Score Box Main Form (c) 2014 Paul F. Michell, Michell Computing. }

{$Mode OBJFPC}
{$LongStrings ON}

Interface

Uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Menus, EmbeddedHTTPServer, fpHTTPServer, fpTemplate, LResources, ThrdTimer;

Type
  TMainForm = Class(TForm)
    MenuItem1: TMenuItem;
    ExitMenuItem: TMenuItem;
    ExitBreak: TMenuItem;
    FullScreenMenuItem: TMenuItem;
    EpeeMenuItem: TMenuItem;
    FullScreenBreak: TMenuItem;
    StartMenuItem: TMenuItem;
    PauseMenuItem: TMenuItem;
    StartBreak: TMenuItem;
    RightFencerMenuItem: TMenuItem;
    LeftFencerMenuItem: TMenuItem;
    SabreMenuItem: TMenuItem;
    FoilMenuItem: TMenuItem;
    PopupMenu1: TPopupMenu;
    WeaponPanel: TPanel;
    TimePanel: TPanel;
    RightOffPanel: TPanel;
    LeftPanel: TPanel;
    CenterPanel: TPanel;
    LeftScorePanel: TPanel;
    LeftOffPanel: TPanel;
    RightScorePanel: TPanel;
    RightPanel: TPanel;
    PistePanel: TPanel;
    LeftFencerPanel: TPanel;
    RightFencerPanel: TPanel;
    Procedure ExitMenuItemClick(Sender: TObject);
    Procedure FoilMenuItemClick(Sender: TObject);
    Procedure FormCreate(Sender: TObject);
    Procedure FormDestroy(Sender: TObject);
    Procedure FormResize(Sender: TObject);
    Procedure FullScreenMenuItemClick(Sender: TObject);
    Procedure LeftFencerMenuItemClick(Sender: TObject);
    Procedure LeftOffPanelClick(Sender: TObject);
    Procedure LeftScorePanelClick(Sender: TObject);
    Procedure EpeeMenuItemClick(Sender: TObject);
    Procedure PauseMenuItemClick(Sender: TObject);
    Procedure RightFencerMenuItemClick(Sender: TObject);
    Procedure RightOffPanelClick(Sender: TObject);
    Procedure RightScorePanelClick(Sender: TObject);
    Procedure SabreMenuItemClick(Sender: TObject);
    Procedure StartMenuItemClick(Sender: TObject);
  Private
    ApplicationFolder: String;
    Timer: TThreadTimer;
    LeftHitTimer: TThreadTimer;
    RightHitTimer: TThreadTimer;
    LeftOffTimer: TThreadTimer;
    RightOffTimer: TThreadTimer;
    EmbeddedServerThread: THTTPServerThread;
    PeriodMinutes: Integer;
    PeriodSecondsRemaining: Integer;
    RemoteParser: TTemplateParser;
    ScoreLeft: Integer;
    ScoreRight: Integer;
    OriginalBounds: TRect;
    OriginalWindowState: TWindowState;
    OriginalBorderStyle: TBorderStyle;
    URLContent: String;
    URLRequest: String;
    ResponseValue: String;
    Procedure FlashPanel(Panel: TPanel; FlashColor: TColor; FlashTimer: TThreadTimer);
    Procedure DoTimer(Sender: TObject);
    Procedure DoLeftTimer(Sender: TObject);
    Procedure DoRightTimer(Sender: TObject);
    Procedure DoLeftOffTimer(Sender: TObject);
    Procedure DoRightOffTimer(Sender: TObject);
    Procedure DoHandleRequest(Sender: TObject;
                              Var ARequest: TFPHTTPConnectionRequest;
                              Var AResponse: TFPHTTPConnectionResponse);
    Procedure ProcessRequest;
  Public
    Procedure StartPeriod;
    Procedure Start;
    Procedure Stop;
    Procedure Pause;
    Procedure Reset;
    Procedure HitLeft;
    Procedure HitRight;
    Procedure DoubleHit;
    Procedure OffTargetLeft;
    Procedure OffTargetRight;
    Procedure AdjustLeft(Value: String);
    Procedure AdjustRight(Value: String);
  End;

Var
  MainForm: TMainForm;

Implementation

{$IfDef WINDOWS}
Uses
  MMSystem;
{$EndIf}

{$R *.lfm}

Procedure TMainForm.FormCreate(Sender: TObject);
Begin
  ApplicationFolder := IncludeTrailingPathDelimiter(ExtractFilePath(ParamStr(0)));
  Timer := TThreadTimer.Create(Self);
  LeftHitTimer := TThreadTimer.Create(Self);
  RightHitTimer := TThreadTimer.Create(Self);
  LeftOffTimer := TThreadTimer.Create(Self);
  RightOffTimer := TThreadTimer.Create(Self);
  Timer.Enabled := False;
  LeftHitTimer.Enabled := False;
  RightHitTimer.Enabled := False;
  LeftOffTimer.Enabled := False;
  RightOffTimer.Enabled := False;
  Timer.OnTimer := @DoTimer;
  LeftHitTimer.OnTimer := @DoLeftTimer;
  RightHitTimer.OnTimer := @DoRightTimer;
  LeftOffTimer.OnTimer := @DoLeftOffTimer;
  RightOffTimer.OnTimer := @DoRightOffTimer;
  Timer.Interval := 1000;
  LeftHitTimer.Interval := 2000;
  RightHitTimer.Interval := 2000;
  LeftOffTimer.Interval := 2000;
  RightOffTimer.Interval := 2000;
  DoubleBuffered := True;
  EmbeddedServerThread := THTTPServerThread.Create(8080, @DoHandleRequest);
  RemoteParser := TTemplateParser.Create;
  //FullScreenMenuItemClick(Self);
  PeriodMinutes := 3;
  Reset;
End;

Procedure TMainForm.FormDestroy(Sender: TObject);
Begin
  EmbeddedServerThread.Terminate;
  Timer.Free;
  LeftHitTimer.Free;
  RightHitTimer.Free;
  LeftOffTimer.Free;
  RightOffTimer.Free;
  RemoteParser.Free;
End;

Procedure TMainForm.FormResize(Sender: TObject);
Var
  PanelMargin: Integer;
  SidePanelWidth: Integer;
  ScorePanelHeight: Integer;
  OffPanelHeight: Integer;
  SmallPanelHeight: Integer;
  ScoreFontHeight: Integer;
  TimeFontHeight: Integer;
  NameFontHeight: Integer;
  InformationFontHeight: Integer;
Begin
  { Calculate the display sizes. }
  PanelMargin := ClientWidth Div 100;
  SidePanelWidth := ClientWidth Div 3;
  ScorePanelHeight := ClientHeight Div 2;
  OffPanelHeight := ScorePanelHeight Div 2;
  SmallPanelHeight := ScorePanelHeight Div 4;
  ScoreFontHeight := (3*ScorePanelHeight) Div 4;
  TimeFontHeight := (2*ScoreFontHeight) Div 3;
  NameFontHeight := ScoreFontHeight Div 3;
  InformationFontHeight := ScoreFontHeight Div 4;
  { Adjust the panel sizes. }
  LeftPanel.Width := SidePanelWidth;
  RightPanel.Width := SidePanelWidth;
  LeftScorePanel.Height := ScorePanelHeight;
  LeftOffPanel.Height := OffPanelHeight;
  RightScorePanel.Height := ScorePanelHeight;
  RightOffPanel.Height := OffPanelHeight;
  PistePanel.Height := SmallPanelHeight;
  LeftFencerPanel.Height := SmallPanelHeight;
  WeaponPanel.Height := SmallPanelHeight;
  RightFencerPanel.Height := SmallPanelHeight;
  { Update the margins. }
  LeftScorePanel.BorderSpacing.Around := PanelMargin;
  LeftOffPanel.BorderSpacing.Around := PanelMargin;
  RightScorePanel.BorderSpacing.Around := PanelMargin;
  RightOffPanel.BorderSpacing.Around := PanelMargin;
  { Update the font heights. }
  TimePanel.Font.Height := TimeFontHeight;
  LeftScorePanel.Font.Height := ScoreFontHeight;
  RightScorePanel.Font.Height := ScoreFontHeight;
  PistePanel.Font.Height := InformationFontHeight;
  LeftFencerPanel.Font.Height := NameFontHeight;
  WeaponPanel.Font.Height := InformationFontHeight;
  RightFencerPanel.Font.Height := NameFontHeight;
End;

Procedure TMainForm.FullScreenMenuItemClick(Sender: TObject);
Begin
  Begin
    If BorderStyle<>bsNone Then
      Begin
        OriginalBorderStyle := BorderStyle;
        OriginalWindowState := WindowState;
        OriginalBounds := BoundsRect;
        BorderStyle := bsNone;
        With Screen.MonitorFromWindow(Handle).BoundsRect Do
          SetBounds(Left, Top, Right-Left, Bottom-Top);
        WindowState := wsFullScreen;
      End
    Else
      Begin
        With OriginalBounds Do
          SetBounds(Left, Top, Right-Left, Bottom-Top);
        BorderStyle := OriginalBorderStyle;
        WindowState := OriginalWindowState
      End;
  End;
End;

Procedure TMainForm.LeftFencerMenuItemClick(Sender: TObject);
Begin
  LeftFencerPanel.Caption := InputBox('Left Fencer','Name:','');
End;

Procedure TMainForm.DoTimer(Sender: TObject);
Begin
  Dec(PeriodSecondsRemaining);
  TimePanel.Caption := FormatDateTime('N:SS', EncodeTime(0, PeriodSecondsRemaining  Div 60, PeriodSecondsRemaining  Mod 60, 0));
  If PeriodSecondsRemaining=0 Then
    Timer.Enabled := False;
End;

Procedure TMainForm.DoLeftTimer(Sender: TObject);
Begin
  LeftScorePanel.Color := clMaroon;
  LeftHitTimer.Enabled := False;
End;

Procedure TMainForm.DoRightTimer(Sender: TObject);
Begin
  RightScorePanel.Color := clGreen;
  RightHitTimer.Enabled := False;
End;

Procedure TMainForm.DoLeftOffTimer(Sender: TObject);
Begin
  LeftOffPanel.Color := clGray;
  LeftOffTimer.Enabled := False;
End;

Procedure TMainForm.DoRightOffTimer(Sender: TObject);
Begin
  RightOffPanel.Color := clGray;
  RightOffTimer.Enabled := False;
End;

Procedure TMainForm.ExitMenuItemClick(Sender: TObject);
Begin
  Close;
End;

Procedure TMainForm.FoilMenuItemClick(Sender: TObject);
Begin
  WeaponPanel.Caption := 'Foil';
  LeftOffPanel.Visible := True;
  RightOffPanel.Visible := True;
End;

Procedure TMainForm.StartPeriod;
Begin
//
End;

Procedure TMainForm.Start;
Begin
  Timer.Enabled := True;
End;

Procedure TMainForm.Stop;
Begin
  Timer.Enabled := False;
  Application.ProcessMessages;
End;

Procedure TMainForm.Pause;
Begin
  Timer.Enabled := Not Timer.Enabled;
End;

Procedure TMainForm.Reset;
Begin
  ScoreLeft := 0;
  ScoreRight := 0;
  PeriodSecondsRemaining := PeriodMinutes*60;
  TimePanel.Caption := FormatDateTime('N:SS', EncodeTime(0, PeriodMinutes, 0, 0));
End;

Procedure TMainForm.HitLeft;
Begin
  If Not LeftHitTimer.Enabled Then
    Begin
      Inc(ScoreLeft);
      LeftScorePanel.Caption := IntToStr(ScoreLeft);
      FlashPanel(LeftScorePanel, clRed, LeftHitTimer);
    End;
End;

Procedure TMainForm.AdjustLeft(Value: String);
Begin
  ScoreLeft += StrToIntDef(Value, 0);
  If ScoreLeft<0 Then
    ScoreLeft := 0;
  LeftScorePanel.Caption := IntToStr(ScoreLeft);
End;

Procedure TMainForm.OffTargetLeft;
Begin
  If Not LeftOffTimer.Enabled  Then
    Begin
      FlashPanel(LeftOffPanel, clWhite, LeftOffTimer);
    End;
End;

Procedure TMainForm.HitRight;
Begin
  If Not RightHitTimer.Enabled Then
    Begin
      Inc(ScoreRight);
      RightScorePanel.Caption := IntToStr(ScoreRight);
      FlashPanel(RightScorePanel, clLime, RightHitTimer);
    End;
End;

Procedure TMainForm.AdjustRight(Value: String);
Begin
  ScoreRight += StrToIntDef(Value, 0);
  If ScoreRight<0 Then
    ScoreRight := 0;
  RightScorePanel.Caption := IntToStr(ScoreRight);
End;

Procedure TMainForm.OffTargetRight;
Begin
  If Not RightOffTimer.Enabled Then
    Begin
      FlashPanel(RightOffPanel, clWhite, RightOffTimer);
    End;
End;

Procedure TMainForm.DoubleHit;
Begin
  HitRight;
  HitLeft;
End;

Procedure TMainForm.FlashPanel(Panel: TPanel; FlashColor: TColor; FlashTimer: TThreadTimer);
Begin
  If Not FlashTimer.Enabled Then
    Begin
      {$IfDef WINDOWS}
      PlaySound(PChar(ApplicationFolder+'Buzzer.wav'),0,SND_ASYNC);
      {$EndIf}
      Panel.Color := FlashColor;
      FlashTimer.Enabled := True;
    End;
End;

Procedure TMainForm.LeftOffPanelClick(Sender: TObject);
Begin
  OffTargetLeft;
End;

Procedure TMainForm.LeftScorePanelClick(Sender: TObject);
Begin
  HitLeft;
End;

Procedure TMainForm.EpeeMenuItemClick(Sender: TObject);
Begin
  WeaponPanel.Caption := 'Epee';
  LeftOffPanel.Visible := False;
  RightOffPanel.Visible := False;
End;

Procedure TMainForm.PauseMenuItemClick(Sender: TObject);
Begin
  Pause;
End;

Procedure TMainForm.RightFencerMenuItemClick(Sender: TObject);
Begin
  RightFencerPanel.Caption := InputBox('Right Fencer','Name:','');
End;

Procedure TMainForm.RightOffPanelClick(Sender: TObject);
Begin
  OffTargetRight;
End;

Procedure TMainForm.RightScorePanelClick(Sender: TObject);
Begin
  HitRight;
End;

Procedure TMainForm.SabreMenuItemClick(Sender: TObject);
Begin
  WeaponPanel.Caption := 'Sabre';
  LeftOffPanel.Visible := False;
  RightOffPanel.Visible := False;
End;

Procedure TMainForm.StartMenuItemClick(Sender: TObject);
Begin
  Start;
End;

Procedure TMainForm.DoHandleRequest(Sender: TObject;
                                    Var ARequest: TFPHTTPConnectionRequest;
                                    Var AResponse: TFPHTTPConnectionResponse);
  Function GetResource(Name: String): String;
  Var
    Resource: TLResource;
  Begin
    Resource := LazarusResources.Find(Name);
    If Resource=Nil Then
      Raise Exception.Create('resource '+Name+' is missing')
    Else
      Result := Resource.Value;
  End;
Begin
  URLContent := Arequest.Content;
  URLRequest := Arequest.URL;
  If (URLRequest='/') Or SameText(URLRequest, '/index.html') Or SameText(URLRequest, '/remote.html') Then
    Begin
      RemoteParser.Values['LeftScore'] := IntToStr(ScoreLeft);
      RemoteParser.Values['RightScore'] := IntToStr(ScoreRight);
      RemoteParser.Values['LeftName'] := LeftFencerPanel.Caption;
      RemoteParser.Values['RightName'] := RightFencerPanel.Caption;
      RemoteParser.Values['Time'] := TimePanel.Caption;
      RemoteParser.Values['Weapon'] := WeaponPanel.Caption;
      AResponse.Content := RemoteParser.ParseString(GetResource('remote'));
    End
  Else If SameText(URLRequest, '/style.css') Then
    Begin
      AResponse.Content := GetResource('style');
    End
  Else If SameText(URLRequest, '/fullscreen.js') Then
    Begin
      AResponse.Content := GetResource('fullscreen');
    End
  Else If SameText(URLRequest, '/ajax.js') Then
    Begin
      AResponse.Content := GetResource('ajax');
    End
  Else If SameText(URLRequest, '/favicon.ico') Then
    Begin
      AResponse.Content := GetResource('favicon');
    End
  Else
    Begin
      EmbeddedServerThread.Synchronize(EmbeddedServerThread, @ProcessRequest);
      AResponse.Content := ResponseValue;
    End;
End;

Procedure TMainForm.ProcessRequest;
Var
  Command: String;
  Value: String;
  Split: longint;
Begin
  Split := Pos('=', URLRequest);
  { Not the start position of 2 to remove the '/' before the command. }
  If Split<>0 Then
    Begin
      Command := LowerCase(Copy(URLRequest, 2, Split-2));
      Value := Copy(URLRequest, Split+1, Length(URLRequest)-Split);
     End
  Else
    Begin
      Command := LowerCase(Copy(URLRequest, 2, Length(URLRequest)-1));
      Value := EmptyStr;
    End;
  Case Command Of
  'time':
    Begin
      ResponseValue := TimePanel.Caption;
    End;
  'start':
    Begin
      Start;
      ResponseValue := TimePanel.Caption;
    End;
  'stop':
    Begin
      Stop;
      ResponseValue := TimePanel.Caption;
    End;
  'pause': Pause;
  'reset': Reset;
  'hitleft':
    Begin
      HitLeft;
      ResponseValue := LeftScorePanel.Caption;
    End;
  'hitright':
    Begin
      HitRight;
      ResponseValue := RightScorePanel.Caption;
    End;
  'offtargetleft': OffTargetLeft;
  'offtargetright': OffTargetRight;
  'doublehit': DoubleHit;
  'adjustleft':
    Begin
      AdjustLeft(Value);
      ResponseValue := LeftScorePanel.Caption;
    End;
  'adjustright':
    Begin
      AdjustRight(Value);
      ResponseValue := RightScorePanel.Caption;
    End;
  End;
End;

Initialization

{$I remote.lrs}

End.
