Unit ThrdTimer;

{ Threaded Timer Unit (c) 2014 Paul F. Michell, Michell Computing. }

{$Mode OBJFPC}
{$LongStrings ON}

Interface

Uses
  Classes, SysUtils{$IfDef Windows}, Windows{$EndIf};

Type
  TThreadTimer = Class(TComponent)
  Private
    FEnabled: Boolean;
    FInterval: Integer;
    FPriority: TThreadPriority;
    FOnTimer: TNotifyEvent;
  Protected
    Procedure SetEnabled(Value: Boolean);
  Public
    Constructor Create(AOwner: TComponent);
  Published
    Property Enabled: boolean read FEnabled write setEnabled;
    Property Interval: integer read FInterval write FInterval Default 1000;
    Property ThreadPriority: TThreadPriority Read FPriority Write FPriority Default tpNormal;
    Property OnTimer: TNotifyEvent Read FOnTimer Write FOnTimer;
  end;
  TThreadTimerThread = Class(TThread)
  Private
    FTimer: TThreadTimer;
  Protected
    Procedure DoExecute;
  Public
    Constructor CreateTimerThread(Timer: TThreadTimer);
    Procedure Execute; Override;
  End;

Implementation

Constructor TThreadTimer.Create(AOwner: TComponent);
Begin
  Inherited;
  FEnabled := False;
  FInterval := 1000;
  FPriority := tpNormal;
End;

Procedure TThreadTimer.SetEnabled(Value: Boolean);
Begin
  If Value<>FEnabled Then
    If Value Then
      Begin
        With TThreadTimerThread.CreateTimerThread(Self) Do
          Begin
            Priority := ThreadPriority;
            FEnabled := True;
            Resume;
          End;
      End
    Else
      FEnabled := False;
End;

Constructor TThreadTimerThread.CreateTimerThread(Timer: TThreadTimer);
Begin
  Inherited Create(true);
  FTimer := Timer;
  FreeOnTerminate := true;
End;

{$IfDef Windows}

Procedure TThreadTimerThread.Execute;
Var
  StartTick: Cardinal;
  Duration: Integer;
Begin
  StartTick := GetTickCount;
  With FTimer Do
    While Enabled Do
      Begin
        Duration := Interval-Integer(GetTickCount-StartTick);
        If Duration<1 Then
          Duration := 1;
        Sleep(Duration);
        Synchronize(@DoExecute);
        StartTick := GetTickCount;
      End;
End;

{$Else}

Procedure TThreadTimerThread.Execute;
Var
  StartTick: QWord;
  Duration: Integer;
Begin
  StartTick := GetTickCount64;
  With FTimer Do
    While Enabled Do
      Begin
        Duration := Interval-Integer(GetTickCount64-StartTick);
        If Duration<1 Then
          Duration := 1;
        Sleep(Duration);
        Synchronize(@DoExecute);
        StartTick := GetTickCount64;
      End;
End;

{$EndIf}

Procedure TThreadTimerThread.DoExecute;
Begin
  With FTimer Do
    If Assigned(FOnTimer) Then
      FOnTimer(FTimer)
End;

End.
