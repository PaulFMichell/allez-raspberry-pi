program Allez;

{ Allez Fencing Score Box System (c) 2014 Paul F. Michell, Michell Computing. }

{$Mode OBJFPC}
{$LongStrings ON}

uses
  {$IfDef UNIX}
  cthreads,
  {$EndIf}
  Interfaces,
  Forms,
  Main;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.

