function QueryFullscreen()
{
if (FullscreenAvailable())
  {  
  document.getElementById('FullscreenDialog').style.display='block';
  }
}
function OpenFullscreen()
{
EnterFullscreen(document.documentElement);  
CloseQueryFullscreen();
}
function CloseQueryFullscreen()
{
document.getElementById('FullscreenDialog').style.display='none';  
}
function FullscreenAvailable() 
{
return (typeof document.fullscreenEnabled != 'undefined') || 
       (typeof document.webkitFullscreenEnabled != 'undefined') ||  
       (typeof document.mozFullScreenEnabled != 'undefined') ||
       (typeof document.msFullscreenEnabled != 'undefined');
}
function EnterFullscreen(element) 
{
  if(element.requestFullscreen) {
    element.requestFullscreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if(element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}
function ExitFullscreen() 
{
  if(document.exitFullscreen) {
    document.exitFullscreen();
  } else if(document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if(document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}