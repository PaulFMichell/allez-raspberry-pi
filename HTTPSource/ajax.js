function ExecuteAndUpdate(Command, ElementID)
{
var xmlhttp;
xmlhttp=new XMLHttpRequest();
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById(ElementID).innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET", Command, true);
xmlhttp.send();
}

function DoLeftPlus()
{
ExecuteAndUpdate("AdjustLeft=1", "LeftScore");
}

function DoLeftMinus()
{
ExecuteAndUpdate("AdjustLeft=-1", "LeftScore");
}

function DoHitLeft()
{
ExecuteAndUpdate("HitLeft", "LeftScore");
}

function DoRightPlus()
{
ExecuteAndUpdate("AdjustRight=1", "RightScore");
}

function DoRightMinus()
{
ExecuteAndUpdate("AdjustRight=-1", "RightScore");
}

function DoHitRight()
{
ExecuteAndUpdate("HitRight", "RightScore");
}

var Timer;

function DoStart()
{
ExecuteAndUpdate("Start", "Time");
Timer = setInterval(function(){ExecuteAndUpdate("Time", "Time")}, 1000);
}

function DoStop()
{
ExecuteAndUpdate("Stop", "Time")
clearInterval(Timer);
}